﻿using System;

namespace Fibonacci
{
    class Program
    {

        public static int Fibonacci(int n)
        {
            int a = 0;
            int b = 1;
            for (int i = 0; i < n; i++)
            {
                int tmp = a;
                a = b;
                b = tmp + b;
            }
            return a;
        }
        static void Main(string[] args)
        {
            for (int i = 0; i < 14; i++)
            {
                Console.Write(Fibonacci(i) + " ");
            }
        }
    }
}
